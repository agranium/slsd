# -*- coding: utf-8 -*-


import requests


class SlackUtil:
    def __init__(self, token, channel_id):
        """
        This is Constructor
        """
        self.token = token
        self.channel_id = channel_id

    def post_message(self, text):
        """
            メッセージを送信する
        """
        params = {
            'token': self.token,
            'channel': self.channel_id,
            'text': text,
            'as_user': 'true'
        }
        r = requests.post(
            r"https://slack.com/api/chat.postMessage",
            params=params
            )

    def channels_history(self):
        """
            チャンネルのメッセージヒストリーを取得する
        """
        params = {
            'token': self.token,
            'channel': self.channel_id
            }
        r = requests.post(
            r"https://slack.com/api/channels.history",
            params=params
            )
        return r.json()['messages']

    def channels_list(self):
        """
            チャンネルリストを取得する
        """
        params = {
            'token': self.token
            }
        r = requests.post(
            r"https://slack.com/api/channels.list",
            params=params
            )
        return r.json()

    def files_upload(self, path, title="ファイル"):
        """
            ファイルをアップロードする
        """
        with open(path, 'rb') as f:
            param = {
                'token': self.token,
                'channels': self.channel_id,
                'title': title
                }
            r = requests.post(
                r"https://slack.com/api/files.upload",
                params=param,
                files={'file': f}
                )
