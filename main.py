import sys
import json
import os.path
import slack_util
import argparse


DEFAULT_CONFIG_FILE = os.path.join(
    os.path.expanduser("~"),
    ".slackconfig.json"
    )
DEFAULT_BOT_NAME = "ANSOR"


if __name__ == "__main__":
    # コンフィグを読み込む
    with open(DEFAULT_CONFIG_FILE) as f:
        config = json.load(f)

    bot_name = DEFAULT_BOT_NAME

    slack = slack_util.SlackUtil(
        config[bot_name]["TOKEN"],
        config[bot_name]["CHANNEL_ID"]
    )

    # for args
    parser = argparse.ArgumentParser(
                prog='slsd',  # プログラム名
                usage='Demonstration of slsd',  # プログラムの利用方法
                description='description',  # 引数のヘルプの前に表示
                epilog='end',  # 引数のヘルプの後で表示
                add_help=True,  # -h/–help オプションの追加
                )
    parser.add_argument('-q', '--quote', help='is quote',
                        action='store_true')
    parser.add_argument('-c', '--code', help='is_code',
                        action='store_true')

    # 引数を解析する
    args = parser.parse_args()

    # main process
    if args.quote:
        text = ">>>{}"
    elif args.code:
        text = "```{}```"
    else:
        text = "{}"

    slack.post_message(text.format(sys.stdin.read()))
